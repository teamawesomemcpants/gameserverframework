'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Game = mongoose.model('Game');

exports.createFromSocket = function(data, cb) {
  var game = new Game(data.message);
    game.owner = data.user._id;
    game.title = data.channel;
    game.description = data.channel;
    game.save(function(err) {
    if (err) console.log(err);
        Game.findOne({
      _id: game._id
    }).populate('owner', 'name username').exec(function(err, game) {
      return cb(game);
    });
  });
};

exports.getAllForSocket = function(channel, cb) {
    Game.find({
    title: channel
  }).sort('created').populate('user', 'name username').exec(function(err, games) {
    return cb(games);
  });
};

exports.getListOfChannels = function(cb) {
  Game.distinct('title', {}, function(err, games) {
    console.log(games)
    return cb(games);
  });
};
