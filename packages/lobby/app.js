'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Lobbies = new Module('lobby');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Lobbies.register(function(app, auth, database) {

  //We enable routing. By default the Package Object is passed to the routes
  Lobbies.routes(app, auth, database);

  //We are adding a link to the main menu for all authenticated users
  Lobbies.menus.add({
    'roles': ['authenticated'],
    'title': 'Lobbies',
    'link': 'all lobbies'
  });
  Lobbies.menus.add({
    'roles': ['authenticated'],
    'title': 'Create New Lobby',
    'link': 'create lobby'
  });

  //Lobbies.aggregateAsset('js','/packages/system/public/services/menus.js', {group:'footer', absolute:true, weight:-9999});
  //Lobbies.aggregateAsset('js', 'test.js', {group: 'footer', weight: -1});

  /*
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Lobbies.settings({'someSetting':'some value'},function (err, settings) {
      //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Lobbies.settings({'anotherSettings':'some value'});

    // Get settings. Retrieves latest saved settings
    Lobbies.settings(function (err, settings) {
      //you now have the settings object
    });
    */
  Lobbies.aggregateAsset('css', 'lobbies.css');

  return Lobbies;
});
