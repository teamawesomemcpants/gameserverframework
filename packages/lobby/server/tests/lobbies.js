'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Game = mongoose.model('Game'),
  Lobby = mongoose.model('Lobby');

/**
 * Globals
 */
var user;
var lobby;
var game;

/**
 * Test Suites
 */
describe('<Unit Test>', function() {
  describe('Model Lobby:', function() {
    beforeEach(function(done) {
      user = new User({
        name: 'Full name',
        email: 'test@test.com',
        username: 'user',
        password: 'password'
      });

      game = new Game({
        title: 'Test',
        description: 'Description',
        ruleset: null,
        owner: user,
        players: [user],
        turn_terminator: 'end_turn'
      });

      user.save(function() {
        game.save(function() {
          lobby = new Lobby({
            title: 'Lobby Title',
            description: 'Lobby Description',
            owner: user,
            players: [user],
            game: game
          });
          done();
        });
      });
    });

    describe('Method Save', function() {
      it('should be able to save without problems', function(done) {
        return lobby.save(function(err) {
          should.not.exist(err);
          lobby.title.should.equal('Lobby Title');
          lobby.description.should.equal('Lobby Description');
          lobby.owner.should.not.have.length(0);
          lobby.created.should.not.have.length(0);
          lobby.players.should.not.have.length(0);
          lobby.game.should.not.have.length(0);
          done();
        });
      });

      it('should be able to show an error when try to save without title', function(done) {
        lobby.title = '';

        return lobby.save(function(err) {
          should.exist(err);
          done();
        });
      });

      it('should be able to show an error when try to save without description', function(done) {
        lobby.description = '';

        return lobby.save(function(err) {
          should.exist(err);
          done();
        });
      });

      it('should be able to show an error when try to save without owner', function(done) {
        lobby.owner = {};

        return lobby.save(function(err) {
          should.exist(err);
          done();
        });
      });

    });

    afterEach(function(done) {
      lobby.remove();
      game.remove();
      user.remove();
      done();
    });
  });
});
