'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Lobby = mongoose.model('Lobby'),
  _ = require('lodash');


/**
 * Find lobby by id
 */
exports.lobby = function(req, res, next, id) {
  Lobby.load(id, function(err, lobby) {
    if (err) return next(err);
    if (!lobby) return next(new Error('Failed to load lobby ' + id));
    req.lobby = lobby;
    next();
  });
};

/**
 * Create an lobby
 */
exports.create = function(req, res) {
  var lobby = new Lobby(req.body);
  lobby.user = req.user;

  lobby.save(function(err) {
    if (err) {
      return res.json(500, {
        error: 'Cannot save the lobby'
      });
    }
    res.json(lobby);

  });
};

/**
 * Update an lobby
 */
exports.update = function(req, res) {
  var lobby = req.lobby;

  lobby = _.extend(lobby, req.body);

  lobby.save(function(err) {
    if (err) {
      return res.json(500, {
        error: 'Cannot update the lobby'
      });
    }
    res.json(lobby);

  });
};

/**
 * Delete an Lobby
 */
exports.destroy = function(req, res) {
  var lobby = req.lobby;

  lobby.remove(function(err) {
    if (err) {
      return res.json(500, {
        error: 'Cannot delete the lobby'
      });
    }
    res.json(lobby);

  });
};

/**
 * Show an Lobby
 */
exports.show = function(req, res) {
  res.json(req.lobby);
};

/**
 * List of Lobbies
 */
exports.all = function(req, res) {
  Lobby.find().sort('-created').populate('owner', 'name username').populate('players', 'username').populate('game', 'title description').exec(function(err, lobbies) {
    if (err) {
      return res.json(500, {
        error: 'Cannot list the lobbies'
      });
    }
    res.json(lobbies);

  });
};
