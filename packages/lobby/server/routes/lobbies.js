'use strict';

var lobbies = require('../controllers/lobbies');

// lobby authorization helpers
var hasAuthorization = function(req, res, next) {
  if (!req.user.isAdmin && req.lobby.user.id !== req.user.id) {
    return res.send(401, 'User is not authorized');
  }
  next();
};

module.exports = function(Lobbies, app, auth) {

  app.route('/lobby')
    .get(lobbies.all)
    .post(auth.requiresLogin, lobbies.create);
  app.route('/lobby/:lobbyId')
    .get(lobbies.show)
    .put(auth.requiresLogin, hasAuthorization, lobbies.update)
    .delete(auth.requiresLogin, hasAuthorization, lobbies.destroy);

  // Finish with setting up the lobbyId param
  app.param('lobbyId', lobbies.lobby);
};
