'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;


/**
 * Lobby Schema
 */
var LobbySchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  title: {
    type: String,
    required: true,
    trim: true
  },
  description: {
    type: String,
    required: true,
    trim: true
  },
  owner: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  players: [{
    type: Schema.ObjectId,
    ref: 'User'
  }
  ],
  game: {
    type: Schema.ObjectId,
    ref: 'Game'
  },
  is_private: {
    type: Boolean,
    default: false
  },
  password: {
    type: String,
    default: ''
  }
});

/**
 * Validations
 */
LobbySchema.path('title').validate(function(title) {
  return !!title;
}, 'Title cannot be blank');

LobbySchema.path('description').validate(function(description) {
  return !!description;
}, 'Description cannot be blank');

/**
 * Statics
 */
LobbySchema.statics.load = function(id, cb) {
  this.findOne({
    _id: id
  }).populate('owner', 'name username').populate('game', 'title description').populate('players', 'username').exec(cb);
};

mongoose.model('Lobby', LobbySchema);
