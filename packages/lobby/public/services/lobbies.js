'use strict';

//Lobbies service used for lobbies REST endpoint
angular.module('mean.lobby').factory('Lobbies', ['$resource',
  function($resource) {
    return $resource('lobby/:lobbyId', {
      lobbyId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
