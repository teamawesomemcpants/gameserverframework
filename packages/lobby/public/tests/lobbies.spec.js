  'use strict';

(function() {
  // Lobbies Controller Spec
  describe('MEAN controllers', function() {
    describe('LobbiesController', function() {
      // The $resource service augments the response object with methods for updating and deleting the resource.
      // If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
      // the responses exactly. To solve the problem, we use a newly-defined toEqualData Jasmine matcher.
      // When the toEqualData matcher compares two objects, it takes only object properties into
      // account and ignores methods.
      beforeEach(function() {
        this.addMatchers({
          toEqualData: function(expected) {
            return angular.equals(this.actual, expected);
          }
        });
      });

      beforeEach(function() {
        module('mean');
        module('mean.system');
        module('mean.lobby');
      });

      // Initialize the controller and a mock scope
      var LobbiesController,
        scope,
        $httpBackend,
        $stateParams,
        $location;

      // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
      // This allows us to inject a service but then attach it to a variable
      // with the same name as the service.
      beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {

        scope = $rootScope.$new();

        LobbiesController = $controller('LobbiesController', {
          $scope: scope
        });

        $stateParams = _$stateParams_;

        $httpBackend = _$httpBackend_;

        $location = _$location_;

      }));

      it('$scope.find() should create an array with at least one lobby object ' +
        'fetched from XHR', function() {

          // test expected GET request
          $httpBackend.expectGET('lobby').respond([{
            title: 'An Lobby for testing',
            description: 'Testing',
            owner: scope.user,
            players: [scope.user],
            game: null
          }]);

          // run controller
          scope.find();
          $httpBackend.flush();

          // test scope value
          expect(scope.lobbies).toEqualData([{
            title: 'An Lobby for testing',
            description: 'Testing',
            owner: scope.user,
            players: [scope.user],
            game: null
          }]);

        });

      it('$scope.findOne() should create an array with one lobby object fetched ' +
        'from XHR using a lobbyId URL parameter', function() {
          // fixture URL parament
          $stateParams.lobbyId = '525a8422f6d0f87f0e407a33';

          // fixture response object
          var testLobbyData = function() {
            return {
              title: 'An Lobby for testing',
              description: 'Testing',
              owner: scope.user,
              players: [scope.user],
              game: null
            };
          };

          // test expected GET request with response object
          $httpBackend.expectGET(/lobby\/([0-9a-fA-F]{24})$/).respond(testLobbyData());

          // run controller
          scope.findOne();
          $httpBackend.flush();

          // test scope value
          expect(scope.lobby).toEqualData(testLobbyData());

        });

      it('$scope.create() with valid form data should send a POST request ' +
        'with the form input values and then ' +
        'locate to new object URL', function() {

          // fixture expected POST data
          var postLobbyData = function() {
            return {
              title: 'An Lobby for testing',
              description: 'Testing',
              owner: scope.user,
              players: [scope.user],
              game: null
            };
          };

          // fixture expected response data
          var responseLobbyData = function() {
            return {
              _id: '525cf20451979dea2c000001',
              title: 'An Lobby for testing',
              description: 'Testing',
              owner: scope.user,
              players: [scope.user],
              game: null
            };
          };

          // fixture mock form input values
          scope.title = 'An Lobby for testing';
          scope.description = 'Testing';
          scope.owner = scope.user;
          scope.players = [scope.user];
          scope.game = null;
          // test post request is sent
          $httpBackend.expectPOST('lobby', postLobbyData()).respond(responseLobbyData());

          // Run controller
          scope.create(true);
          $httpBackend.flush();

          // test form input(s) are reset
          expect(scope.title).toEqual('');
          expect(scope.description).toEqual('');

          // test URL location to new object
          expect($location.path()).toBe('/lobbies/' + responseLobbyData()._id);
        });

      it('$scope.update(true) should update a valid lobby', inject(function(Lobbies) {

        // fixture rideshare
        var putLobbyData = function() {
          return {
              _id: '525a8422f6d0f87f0e407a33',
              title: 'An Lobby for testing',
              description: 'Testing',
              owner: scope.user,
              players: [scope.user],
              game: null
            };
        };

        // mock lobby object from form
        var lobby = new Lobbies(putLobbyData());

        // mock lobby in scope
        scope.lobby = lobby;

        // test PUT happens correctly
        $httpBackend.expectPUT(/lobby\/([0-9a-fA-F]{24})$/).respond();

        // run controller
        scope.update(true);
        $httpBackend.flush();

        // test URL location to new object
        expect($location.path()).toBe('/lobbies/' + putLobbyData()._id);

      }));

      it('$scope.remove() should send a DELETE request with a valid lobbyId ' +
        'and remove the lobby from the scope', inject(function(Lobbies) {

          // fixture rideshare
          var lobby = new Lobbies({
            _id: '525a8422f6d0f87f0e407a33'
          });

          // mock rideshares in scope
          scope.lobbies = [];
          scope.lobbies.push(lobby);

          // test expected rideshare DELETE request
          $httpBackend.expectDELETE(/lobby\/([0-9a-fA-F]{24})$/).respond(204);

          // run controller
          scope.remove(lobby);
          $httpBackend.flush();

          // test after successful delete URL location lobbys list
          //expect($location.path()).toBe('/lobbys');
          expect(scope.lobbies.length).toBe(0);

        }));
    });
  });
}());
