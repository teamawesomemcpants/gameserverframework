'use strict';

angular.module('mean.lobby').controller('LobbiesController', ['$scope', '$stateParams', '$location', 'Global', 'Lobbies',
  function($scope, $stateParams, $location, Global, Lobbies) {
    $scope.global = Global;

    $scope.hasAuthorization = function(lobby) {
      if (!lobby || !lobby.user) return false;
      return $scope.global.isAdmin || lobby.owner._id === $scope.global.user._id;
    };

    $scope.create = function(isValid) {
      if (isValid) {
        var lobby = new Lobbies({
          title: this.title,
          description: this.description,
          players: this.players,
          game: this.game
        });
        lobby.$save(function(response) {
          $location.path('lobby/' + response._id);
        });

        this.title = '';
        this.description = '';
        this.players = '';
        this.game = '';
      } else {
        $scope.submitted = true;
      }
    };

    $scope.remove = function(lobby) {
      if (lobby) {
        lobby.$remove();

        for (var i in $scope.lobbies) {
          if ($scope.lobbies[i] === lobby) {
            $scope.lobbies.splice(i, 1);
          }
        }
      } else {
        $scope.lobby.$remove(function(response) {
          $location.path('lobbies');
        });
      }
    };

    $scope.update = function(isValid) {
      if (isValid) {
        var lobby = $scope.lobby;
        if (!lobby.updated) {
          lobby.updated = [];
        }
        lobby.updated.push(new Date().getTime());

        lobby.$update(function() {
          $location.path('lobbies/' + lobby._id);
        });
      } else {
        $scope.submitted = true;
      }
    };

    $scope.find = function() {
      Lobbies.query(function(lobbies) {
        $scope.lobbies = lobbies;
      });
    };

    $scope.findOne = function() {
      Lobbies.get({
        lobbyId: $stateParams.lobbyId
      }, function(lobby) {
        $scope.lobby = lobby;
      });
    };
  }
]);
