'use strict';

//Setting up route
angular.module('mean.lobby').config(['$stateProvider',
  function($stateProvider) {
    // Check if the user is connected
    var checkLoggedin = function($q, $timeout, $http, $location) {
      // Initialize a new promise
      var deferred = $q.defer();

      // Make an AJAX call to check if the user is logged in
      $http.get('/loggedin').success(function(user) {
        // Authenticated
        if (user !== '0') $timeout(deferred.resolve);

        // Not Authenticated
        else {
          $timeout(deferred.reject);
          $location.url('/login');
        }
      });

      return deferred.promise;
    };

    // states for my app
    $stateProvider
      .state('all lobbies', {
        url: '/lobbies',
        templateUrl: 'lobby/views/list.html',
        resolve: {
          loggedin: checkLoggedin
        }
      })
      .state('create lobby', {
        url: '/lobbies/create',
        templateUrl: 'lobby/views/create.html',
        resolve: {
          loggedin: checkLoggedin
        }
      })
      .state('edit lobby', {
        url: '/lobbies/:lobbyId/edit',
        templateUrl: 'lobby/views/edit.html',
        resolve: {
          loggedin: checkLoggedin
        }
      })
      .state('lobby by id', {
        url: '/lobbies/:lobbyId',
        templateUrl: 'lobby/views/view.html',
        resolve: {
          loggedin: checkLoggedin
        }
      });
  }
]);
