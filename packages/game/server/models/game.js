'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;


/**
 * Game Schema
 */
var GameSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  title: {
    type: String,
    required: true,
    trim: true
  },
  description: {
    type: String,
    required: true,
    trim: true
  },
  owner: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  players: [{
    type: Schema.ObjectId,
    ref: 'User'
  }],
  password: {
    type: String,
    default: ''
  },
  is_private: {
    type: Boolean,
    default: false
  },
  player_limit: {
    type: Number,
    default: 6,
  },
  ruleset: {
    type: Schema.ObjectId,
    ref: 'RuleSet'
  }
});

/**
 * Validations
 */
GameSchema.path('title').validate(function(title) {
  return !!title;
}, 'Title cannot be blank');

GameSchema.path('description').validate(function(description) {
  return !!description;
}, 'Description cannot be blank');

/**
 * Statics
 */
GameSchema.statics.load = function(id, cb) {
  this.findOne({
    _id: id
  }).populate('owner', 'name username').populate('ruleset', 'name').exec(cb);
};

mongoose.model('Game', GameSchema);
