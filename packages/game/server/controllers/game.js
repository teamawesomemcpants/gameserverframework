'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Game = mongoose.model('Game'),
  _ = require('lodash');


/**
 * Find game by id
 */
exports.game = function(req, res, next, id) {
  Game.load(id, function(err, game) {
    if (err) return next(err);
    if (!game) return next(new Error('Failed to load game ' + id));
    req.game = game;
    next();
  });
};

/**
 * Create an game
 */
exports.create = function(req, res) {
  var game = new Game();
  game.title = req.body.title;
  game.description = req.body.description;
  game.password = req.body.password;
  game.is_private = req.body.is_private;
  game.player_limit = req.body.player_limit;
  if(req.body.ruleset) {
      game.ruleset._id = req.body.ruleset._id;
  }
  else {
      game.ruleset = null;
  }
  game.owner = req.body.owner._id;

  game.save(function(err) {
    if (err) {
      console.log(err);
      return res.json(500, {
        error: 'Cannot save the game',
        details: err
      });
    }
    res.json(game);

  });
};

/**
 * Update an game
 */
exports.update = function(req, res) {
  var game = req.game;

  game = _.extend(game, req.body);

  game.save(function(err) {
    if (err) {
      return res.json(500, {
        error: 'Cannot update the game'
      });
    }
    res.json(game);

  });
};

/**
 * Delete an game
 */
exports.destroy = function(req, res) {
  var game = req.game;

  game.remove(function(err) {
    if (err) {
      return res.json(500, {
        error: 'Cannot delete the game'
      });
    }
    res.json(game);

  });
};

/**
 * Show an game
 */
exports.show = function(req, res) {
  res.json(req.game);
};

/**
 * List of Games
 */
exports.all = function(req, res) {
  Game.find().sort('-created').populate('user', 'name username').populate('ruleset', 'name').exec(function(err, games) {
    if (err) {
      return res.json(500, {
        error: 'Cannot list the games'
      });
    }
    res.json(games);

  });
};
