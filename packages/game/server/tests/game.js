'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Game = mongoose.model('Game'),
  RuleSet = mongoose.model('RuleSet');

/**
 * Globals
 */
var user;
var game;
var ruleset;
/**
 * Test Suites
 */
describe('<Unit Test>', function() {
  describe('Model Game:', function() {
    beforeEach(function(done) {
      user = new User({
        name: 'Full name',
        email: 'test@test.com',
        username: 'user',
        password: 'password'
      });

      ruleset = new RuleSet({
        title: 'Test',
        custom_state: { test: true },
        async: false,
        valid_timelimits: [1],
        player_interaction: false,
        limit_turn_actions: false
      });

      user.save(function() {
        ruleset.save(function(){
          game = new Game({
            title: 'Game Title',
            description: 'Game Description',
            owner: user,
            players: [user],
            password: '',
            is_private: false,
            player_limit: 5,
            ruleset: ruleset
          });
          //console.log(user);
          //console.log(ruleset);
          //console.log(game);
          done();
        });
      });
    });

    describe('Method Save', function() {
      it('should be able to save without problems', function(done) {
        //console.log(game);
        return game.save(function(err) {
          //console.log(game);
          should.not.exist(err);
          game.title.should.equal('Game Title');
          game.description.should.equal('Game Description');
          game.owner.should.not.have.length(0);
          game.created.should.not.have.length(0);
          game.players.should.not.have.length(0);
          game.is_private.should.equal(false);
          game.player_limit.should.equal(5);
          game.password.should.equal('');
          game.ruleset.should.not.have.length(0);
          done();
        });
      });

      it('should be able to show an error when try to save without title', function(done) {
        game.title = '';
        return game.save(function(err) {
          should.exist(err);
          done();
        });
      });

      it('should be able to show an error when try to save without content', function(done) {
        game.description = '';
        return game.save(function(err) {
          should.exist(err);
          done();
        });
      });

      it('should be able to show an error when try to save without owner', function(done) {
        game.owner = {};
        return game.save(function(err) {
          should.exist(err);
          done();
        });
      });

    });

    afterEach(function(done) {
      //console.log('gremove');
      game.remove();
      //console.log('rremove');
      ruleset.remove();
      //console.log('uremvoe');
      user.remove();
      done();
    });
  });
});
