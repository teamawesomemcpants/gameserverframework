'use strict';

var games = require('../controllers/game');

// Game authorization helpers
var hasAuthorization = function(req, res, next) {
  //if (!req.user.isAdmin && req.game.user.id !== req.user.id) {
  //  return res.send(401, 'User is not authorized');
  //}
  next();
};

module.exports = function(Games, app, auth) {

  app.route('/game')
    .get(games.all);
    //.post(auth.requiresLogin, games.create);
  app.route('/game/:gameId')
    .get(games.show);
    //.put(hasAuthorization, games.update);
    //.delete(auth.requiresLogin, hasAuthorization, games.destroy);

  // Finish with setting up the gameId param
  app.param('gameId', games.game);
};
