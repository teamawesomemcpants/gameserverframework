angular
.module('mean.game')
.directive('game', function() {
    'use strict';
    return {
        restrict: 'E',
        link: function (scope, element, attributes) {
            Storage.prototype.setObject = function(key, value) {
              console.log(value);
              this.setItem(key, JSON.stringify(value, function(key, value){
                if(key === 'player') {
                  return value.name;
                } else if(key === 'group' || key === 'world' || key === 'sprite' || key === 'game'){
                  return value.id;
                } else {
                  return value;
                }
              }));
            };

            Storage.prototype.getObject = function(key) {
              var value = this.getItem(key);
              return value && JSON.parse(value);
            };

            function endTurn() {
              activePlayer.fillHand();
              activePlayer.changeMagic(activePlayer.magic * -1);
              activePlayer.changeElemental(activePlayer.elemental * -1);
              currentTurn += 1;
              if(currentTurn % 2 === 0) {
                counters.turn.text = currentTurn/2;
              }
              rotatePlayer();
              counters.currentPlayer.text = activePlayer.name;
            }

            function scaleCard(card) {
              card.scale.y = 1 / (card.height / cardHeight);
              card.scale.x = card.scale.y;
              return card;
            }

            function getTextureOrKey(name) {
              if (cardTypes[name] !== undefined && cardTypes[name].frame) {
                return cardTypes[name].getTexture();
              } else {
                return name;
              }
            }

            function addCard(name) {
              var detailViewer;
              var texture = getTextureOrKey(name);
              var cardSprite = mainSpace.create(0, 0, texture);
              cardSprite.inputEnabled = true;
              cardSprite.events.onInputUp.add(function(sprite) {
                if (
                    (!cardSprite.input.isDragged) ||
                    ((Math.abs(sprite.x - sprite.originalPosition.x) < (sprite.width * 0.1)) && (Math.abs(sprite.y - sprite.originalPosition.y) < (sprite.height * 0.2)))
                  ) {
                  // TODO only face-up cards
                  if (detailViewer) { detailViewer.destroy(); }
                  detailViewer = mainSpace.create(0, 0, texture);
                  detailViewer.scale.y = 1 / (detailViewer.height / game.height);
                  detailViewer.scale.x = detailViewer.scale.y;
                  game.input.onDown.addOnce(function() { detailViewer.destroy(); });
                }
              });
              return scaleCard(cardSprite);
            }

            function Deck(options) {
              angular.extend(this, options);
              this.cards = [];
              if (this.type === undefined) {
                this.type = 'main';
              }
            }

            Deck.prototype.shuffle = function() {
              var newCards = [];
              var oldCards = [];

              angular.forEach(this.cards, function(card,index) {
                oldCards.push(card);
              });

              angular.forEach(this.cards, function(card, index) {
                var pos = Math.floor(Math.random() * oldCards.length);
                newCards.push(oldCards[pos]);
                oldCards.splice(pos, 1);
              }, this);

              this.cards = newCards;
              return this.cards;
            };

            Deck.prototype.draw = function() {
              var card;
              if (this.cards.length <= 0 && this.type === 'main') {
                while (this.player.discardDeck.cards.length) {
                  this.addCard(this.player.discardDeck.draw());
                }
              }
              card = this.cards.pop() || null;
              var newSprite;
              if (this.cards.length <= 0 && this.type !== 'worldPlayer') {
                newSprite = addCard('emptyDeck');
                newSprite.x = this.sprite.x;
                newSprite.y = this.sprite.y;
                this.sprite.destroy();
                this.sprite = newSprite;
              } else if (this.topVisible) {
                var topCard = this.cards[this.cards.length-1];
                newSprite = addCard(topCard.name);
                newSprite.x = this.sprite.x;
                newSprite.y = this.sprite.y;
                this.sprite.destroy();
                this.sprite = newSprite;
              }
              return card;
            };

            Deck.prototype.flipTop = function() {
              var card = this.cards[this.cards.length-1];
              this.sprite.destroy();
              this.sprite = addCard(card.name);
              this.sprite.x = mainSpace.maxX - cardWidth;
            };

            Deck.prototype.addCard = function(card) {
              this.cards.push(card);

              if (this.type !== 'worldPlayer') {
                var newSprite;
                if (this.type === 'discard') {
                  newSprite = addCard(card.name);
                  newSprite.x = this.sprite.x;
                  newSprite.y = this.sprite.y;
                  this.sprite.destroy();
                  this.sprite = newSprite;
                }
                else if (this.cards.length <= 0) {
                  newSprite = addCard('emptyDeck');
                  newSprite.x = this.sprite.x;
                  newSprite.y = this.sprite.y;
                  this.sprite.destroy();
                  this.sprite = newSprite;
                }
                else {
                  newSprite = addCard('cardBack');
                  newSprite.x = this.sprite.x;
                  newSprite.y = this.sprite.y;
                  this.sprite.destroy();
                  this.sprite = newSprite;
                }
              }

              return this.cards;
            };

            Deck.prototype.receiveCard = function() {
              var playerTotals = {};
              angular.forEach(players, function(player) {
                var totals = {magic: 0, elemental: 0};
                angular.forEach(player.worldDeck.cards, function(card) {
                  totals.magic += card.magic || 0;
                  totals.elemental += card.elemental || 0;
                });
                playerTotals[player.name] = totals;
                this.counters[player.name].magic.text = totals.magic;
                this.counters[player.name].elemental.text = totals.elemental;
              }, this);

              function displayWinScreen(player) {
                var winScreen = game.add.sprite(20, 20, player + '-wins');
                winScreen.width = 944;
                winScreen.height = 688;
                winScreen.visible = true;
                winScreen.bringToTop();
              }

              var winner;
              var topCard = this.cards[this.cards.length-1];
              angular.forEach(playerTotals, function(totals, playerName) {
                var topCardMagicCost = topCard.magicCost || 0;
                var topCardElementalCost = topCard.elementalCost || 0;
                if (totals.magic >= topCardMagicCost && totals.elemental >= topCardElementalCost) {
                  winner = players[playerName];
                }
              }, this);
              if (winner) {
                worldsClaimed += 1;
                counters.worldsClaimed.text = worldsClaimed + '/' + worldsCount;
                winner.changeVictoryPoints(topCard.victoryPoints);
                angular.forEach(players, function(player) {
                  while (player.worldDeck.cards.length) {
                    player.discardDeck.addCard(player.worldDeck.draw());
                  }
                  this.counters[player.name].magic.text = 0;
                  this.counters[player.name].elemental.text = 0;
                }, this);
                this.draw();
                if (this.cards.length < 1) {
                  // TODO game over whaaaaaaat

                  //var endGameText = game.add.text(game.world.centerX, 400, '- click to start -', { font: "60px Arial", fill: "#ffffff", align: "center" });
                  //endGameText.anchor.setTo(0.5, 0.5);
                  //endGameText.text = 'Game Over! ' + activePlayer.name + ' has ' + activePlayer.victoryPoints;
                  //endGameText.visible = true;

                  if(players.Thor.victoryPoints > players.Loki.victoryPoints) {
                    displayWinScreen('thor');
                  } else if(players.Thor.victoryPoints < players.Loki.victoryPoints) {
                    displayWinScreen('loki');
                  } else {
                    var ThorCardCount = players.Thor.deck.cards.length + players.Thor.discardDeck.cards.length;
                    var LokiCardCount = players.Loki.deck.cards.length + players.Loki.discardDeck.cards.length;
                    if(ThorCardCount > LokiCardCount ) {
                      displayWinScreen('thor');
                    } else {
                      displayWinScreen('loki');
                    }
                  }
                  endTurnButton.visible = false;
                  endTurnButton.inputEnabled = false;
                  var newGameFunc = function(button) {
                    //game.destroy();
                    //game = new Phaser.Game(1024, 768, Phaser.AUTO, 'game', {preload: preload, create: create, update: update});
                    angular.forEach(players, function(player){
                      player.victoryPoints = 0;
                    });
                    game.state.restart();
                  };
                  var newGameButton = new Phaser.Button(game, endTurnButton.x, endTurnButton.y, 'newGameButton', newGameFunc);
                  newGameButton.width = endTurnButton.width;
                  newGameButton.height = endTurnButton.height;
                  endTurnButtonGroup.remove(endTurnButton);
                  endTurnButtonGroup.add(newGameButton);
                  newGameButton.visible = true;
                  newGameButton.inputEnabled = true;
                  console.log('RAAAAAGNAAAAAAROOOOOOOOK');
                }
              }
            };

            Deck.prototype.purchase = function() {

              function canAfford(player, costs) {
                return activePlayer.magic >= costs.magic && activePlayer.elemental >= costs.elemental;
              }

              var card;
              if (this.type === 'buy') {
                if (canAfford(activePlayer, {magic: this.fixedCardType.magicCost, elemental: this.fixedCardType.elementalCost})) {
                  activePlayer.changeMagic(this.fixedCardType.magicCost * -1);
                  activePlayer.changeElemental(this.fixedCardType.elementalCost * -1);
                  card = this.draw();
                  activePlayer.discardDeck.addCard(card);
                }
              }
            };

            function Card(type) {
              this.type = type;
              this.elemental = this.type.elemental;
              this.magic = this.type.magic;
              this.name = this.type.name;
              this.magicCost = this.type.magicCost;
              this.elementalCost = this.type.elementalCost;
              this.victoryPoints = this.type.victoryPoints;
            }

            Card.prototype.play = function() {
              if (this.magic !== undefined) {
                activePlayer.changeMagic(this.magic);
              }
              if (this.elemental !== undefined) {
                activePlayer.changeElemental(this.elemental);
              }
              activePlayer.discardDeck.addCard(this);
            };

            Card.prototype.commit = function() {
              activePlayer.worldDeck.addCard(this);
              worldDeck.receiveCard();
            };

            function CardType(options) {
              this.magic=0;
              this.elemental=0;
              this.magicCost=0;
              this.elementalCost=0;
              this.victoryPoints=0;
              angular.extend(this, options);
            }

            CardType.prototype.getTexture = function() {

              function center(what, x, y) {
                if (x) {
                  what.x = x - what.width / 2;
                }
                if (y) {
                  what.y = y - what.height / 2;
                }

                return what;
              }

              if (this._texture === undefined) {

                var frame = new Phaser.Image(game, 0, 0, 'frame/'+this.frame);
                // components
                var renderDestination = new Phaser.RenderTexture(game, frame.width, frame.height);

                var group = new Phaser.Group(game, null);
                group.add(frame);
                group.add(new Phaser.Image(game, 77, 160, 'art/'+this.name));
                group.add(new Phaser.Text(game, 100, 105, this.title, {fill: 'white', font: 'bold 30px gunfjaun_runicregular'}));
                group.add(new Phaser.Text(game, 100, 835, this.title, {fill: 'white', font: 'bold 30px Arial'}));

                if (this.rulesText) {
                  var rulesText = group.add(new Phaser.Text(game, 100, 990, this.rulesText || 'rules go here', {fill: 'white'}));
                  rulesText.wordWrap = true;
                  rulesText.wordWrapWidth = 300;
                }

                if (this.flavorText) {
                  // TODO italic gets cut off; c'mon
                  var flavorText = group.add(new Phaser.Text(game, 100, 905, this.flavorText || 'flavah', {fill: 'white', font: 'italic 24px Arial'}));
                  flavorText.wordWrap = true;
                  flavorText.wordWrapWidth = 300;
                }

                if (this.magicCost || this.elementalCost) {
                  var cost = group.add(new Phaser.Text(game, 0, 0, '', {fill: 'white', font: '48px Arial'}));

                  if (this.frame.indexOf('magic') > -1) {
                    cost.text = this.magicCost;
                  } else {
                    cost.text = this.elementalCost;
                  }

                  center(cost, 579, 964);

                }

                // positioning
                renderDestination.renderXY(group, 0, 0, true);

                this._texture = renderDestination;
              }

              return this._texture;
            };

            function Player(options) {
              angular.extend(this, options);
              this.name = options.name;
              this.handSize = 4;
              this.magic = 0;
              this.elemental = 0;
              this.victoryPoints = 0;
              this.playLog = {};
            }

            function EnableDragAndDrop(sprite) {
              sprite.inputEnabled = true;
              sprite.input.enableDrag(false, true);
              sprite.events.onDragStart.add(startDrag, this);
              sprite.events.onDragStop.add(stopDrag, this);
            }

            function DisableDragAndDrop(sprite) {
                sprite.inputEnable = false;
            }

            Player.prototype.activate = function() {
              activePlayer = this;
              this.card.visible = true;
              this.deck.sprite.visible = true;
              this.hand.group.visible = true;
              this.card.visible = true;
              this.deck.sprite.visible = true;
              this.discardDeck.sprite.visible = true;

              counters.elemental.text = this.elemental;
              counters.magic.text = this.magic;
              counters.victoryPoints.text = this.victoryPoints;
            };

            Player.prototype.changeElemental = function(n) {
              this.elemental += n;
              counters.elemental.text = this.elemental;
            };

            Player.prototype.changeMagic = function(n) {
              this.magic += n;
              counters.magic.text = this.magic;
            };

            Player.prototype.changeVictoryPoints = function(n) {
              this.victoryPoints += n;
              counters.victoryPoints.text = this.victoryPoints;
            };

            Player.prototype.deactivate = function() {
              this.card.visible = false;
              this.deck.sprite.visible = false;
              this.hand.group.visible = false;
              this.discardDeck.sprite.visible = false;
            };

            Player.prototype.createHook = function() {
              this.card = addCard(this.name);
              this.card.y = mainSpace.maxY - this.card.height;

              this.deck = new Deck({ player: this });

              this.deck.sprite = addCard('cardBack');
              this.deck.sprite.x = this.card.x + this.card.width;
              this.deck.sprite.y = this.card.y;

              angular.forEach(this.openingDeck, function(count, name) {
                var type = cardTypes[name];

                for (var i = 0; i < count; i += 1) {
                  var card = new Card(type);
                  this.deck.cards.push(card);
                }
              }, this);

              this.deck.shuffle();
              this.hand = new Hand({player: this});

              this.hand.group.x = this.deck.sprite.x + cardWidth;
              this.hand.group.y = this.deck.sprite.y;
              this.fillHand();

              this.discardDeck = new Deck({type: 'discard'});
              this.discardDeck.sprite = addCard('emptyDeck');
              this.discardDeck.sprite.x = game.width - cardWidth;
              this.discardDeck.sprite.y = cardHeight;

              this.worldDeck = new Deck({type: 'worldPlayer'});
            };

            Player.prototype.fillHand = function() {
              for (var i = this.hand.cards.length; i < this.handSize; i += 1) {
                var card = this.deck.draw();
                if (card !== null) {
                  this.hand.addCard(card);
                }
              }
            };

            function Hand(options) {
              angular.extend(this, options);
              this.group = mainSpace.add(new Phaser.Group(game));
              this.cards = [];
            }

            function startDrag(sprite) {
              sprite.originalPosition = sprite.position.clone();
            }

            function stopDrag(sprite) {

              function checkIntersection(sprite, deck) {
                var world = sprite.world;
                var deckWorld = deck.sprite.world;

                var xOver = Math.max(cardWidth - Math.abs(world.x - deckWorld.x), 0);
                var yOver = Math.max(cardHeight - Math.abs(world.y - deckWorld.y), 0);

                return xOver * yOver;
              }

              var anim = game.add.tween(sprite);
              var worldIntersection = checkIntersection(sprite, worldDeck);
              var discardIntersection = checkIntersection(sprite, activePlayer.discardDeck);

              if (worldIntersection > 0 || discardIntersection > 0) {
                if (worldIntersection > discardIntersection) {
                  return 'world';
                } else if (discardIntersection > 0) {
                  return 'discard';
                }
              } else {
                anim.to({y: sprite.originalPosition.y, x: sprite.originalPosition.x}, 200 + Math.random() * 30, Phaser.Easing.Linear.k);
                anim.start();
                return false;
              }
            }

            Hand.prototype.addCard = function(card) {
              this.cards.push(card);
              this.rerender();
            };

            Hand.prototype.rerender = function() {
              this.group.destroy(true, true);
              angular.forEach(this.cards, function(card, index) {
                var sprite = addCard(card.name);
                sprite.x = index * cardWidth;
                sprite.inputEnabled = true;
                sprite.input.enableDrag();
                sprite.events.onDragStart.add(startDrag, this);
                sprite.events.onDragStop.add(function(sprite) {
                  if (stopDrag(sprite) === 'discard') {
                    card.play();
                    this.cards.splice(this.cards.indexOf(card), 1);
                    this.rerender();
                  } else if (stopDrag(sprite) === 'world') {
                    card.commit();
                    sprite.destroy();
                    this.cards.splice(this.cards.indexOf(card), 1);
                    this.rerender();
                  }
                }, this);
                this.group.add(sprite);
              }, this);
            };

            function setPlayer(player) {
              angular.forEach(players, function(player) {
                player.deactivate();
              });
              player.activate();
            }

            function rotatePlayer() {
              var playerNames = Object.keys(players);
              var currentPlayerIndex = playerNames.indexOf(activePlayer.name);
              var newPlayerIndex = (currentPlayerIndex >= (playerNames.length-1)) ? 0 : (currentPlayerIndex + 1);
              setPlayer(players[playerNames[newPlayerIndex]]);
            }

            var activePlayer;
            var cardHeight;
            var cardWidth;
            var buyCardWidth;
            var bottomBarCardWidth;

            var cardTypes = {
              magic: new CardType({name: 'magic', title: 'Magic', magic: 1, frame: 'magic', rulesText: '+1 magic'}),
              elemental: new CardType({name: 'elemental', title: 'Elemental', elemental: 1, frame: 'elemental', rulesText: '+1 elemental'}),
              mjolnir: new CardType({name: 'mjolnir', title: 'Mjolnir', flavorText: 'hammer of Thor', rulesText: '+3 elemental', elementalCost: 2, elemental: 3, frame: 'elemental_with_cost'}),
              megingjord: new CardType({name: 'megingjord', title: 'Megingjord', flavorText: 'belt of Thor', rulesText: '+2 elemental', elementalCost: 1, elemental: 2, frame: 'elemental_with_cost'}),
              jarngreipr: new CardType({name: 'jarngreipr', title: 'Jarngreipr', flavorText: 'gloves of Thor', rulesText: '+2 elemental', elementalCost: 1, elemental: 2, frame: 'elemental_with_cost'}),
              gridarvolr: new CardType({name: 'gridarvolr', title: 'Gridarvolr', flavorText: 'staff of greed', rulesText: '+2 elemental', elementalCost: 1, elemental: 2, frame: 'elemental_with_cost'}),

              hel: new CardType({name: 'hel', title: 'Hel', flavorText: 'Ruler of the Dead', rulesText: '+4 magic', magicCost: 3, magic: 4, frame: 'magic_with_cost'}),
              jormungandr: new CardType({name: 'jormungandr', title: 'Jormungandr', flavorText: 'Serpent son of Loki', rulesText: '+2 magic', magicCost: 1, magic: 2, frame: 'magic_with_cost'}),
              sexChange: new CardType({name: 'sexChange', title: 'Sex Change', flavorText: 'because Loki can be whatever she wants', rulesText: '+3 magic', magicCost: 2, magic: 3, frame: 'magic_with_cost'}),
              fenrir: new CardType({name: 'fenrir', title: 'Fenrir', flavorText: 'Wolf son of Loki', rulesText: '+2 magic', magicCost: 1, magic: 2, frame: 'magic_with_cost'}),

              asgard: new CardType({name: 'asgard', magicCost: 1, elementalCost: 1, victoryPoints: 1, frame: false}),
              midgard: new CardType({name: 'midgard', magicCost: 1, elementalCost: 1, victoryPoints: 1, frame: false}),
              helWorld: new CardType({name: 'helWorld', magicCost: 1, elementalCost: 1, victoryPoints: 1, frame: false}),
              jotunheim: new CardType({name: 'jotunheim', magicCost: 1, elementalCost: 1, victoryPoints: 1, frame: false}),
              niflheimr: new CardType({name: 'niflheimr', magicCost: 1, elementalCost: 1, victoryPoints: 1, frame: false}),
            };

            var buyDecks = [];
            var discardDeck;
            var worldDeck;
            var mainSpace;
            var currentTurn = 1;
            var worldsClaimed = 0;
            var worldsCount = 0;
            var counters = {};
            var worldCounter;
            var gameLog = {};
            var endTurnButton;
            var endTurnButtonLabel;
            var endTurnButtonGroup;

            var players = {
              Loki: new Player({
                name: 'Loki',
                openingDeck: {magic: 3, elemental: 2},
                buyDecks: ['fenrir', 'jormungandr', 'sexChange', 'hel']
              }),
              Thor: new Player({
                name: 'Thor',
                openingDeck: {magic: 2, elemental: 3},
                buyDecks: ['gridarvolr', 'megingjord', 'jarngreipr', 'mjolnir']
              })
            };

            function preload() {
              angular.forEach(players, function(player) {
                game.load.image(player.name, '/game/assets/img/'+player.name.toLowerCase()+'.png');
              });

              game.load.image('cardBack', '/game/assets/img/card_back_2.png');
              game.load.image('worldCardBack', '/game/assets/img/card_back.png');
              game.load.image('emptyDeck', '/game/assets/img/card_back_discard.png');
              game.load.image('magic', '/game/assets/img/loki_1.png');
              game.load.image('elemental', '/game/assets/img/elemental.png');
              game.load.image('mjolnir', '/game/assets/img/mjolnir.png');
              game.load.image('megingjord', '/game/assets/img/megingjord.png');
              game.load.image('jarngreipr', '/game/assets/img/jarngreipr.png');
              game.load.image('gridarvolr', '/game/assets/img/gridarvolr.png');
              game.load.image('elemental_icon', '/game/assets/img/elemental_icon.png');
              game.load.image('magic_icon', '/game/assets/img/magic_icon.png');
              game.load.image('hel', '/game/assets/img/hel.png');
              game.load.image('jormungandr', '/game/assets/img/jormungandr.png');
              game.load.image('sexChange', '/game/assets/img/sex_change.png');
              game.load.image('fenrir', '/game/assets/img/fenrir.png');

              game.load.image('midgard', '/game/assets/img/world/midgard.png');
              game.load.image('asgard', '/game/assets/img/world/asgard.png');
              game.load.image('helWorld', '/game/assets/img/world/hel.png');
              game.load.image('jotunheim', '/game/assets/img/world/jotunheim.png');
              game.load.image('niflheimr', '/game/assets/img/world/niflheimr.png');

              game.load.image('frame/thor', '/game/assets/img/frames/thor.png');
              game.load.image('frame/loki', '/game/assets/img/frames/loki.png');
              game.load.image('frame/magic', '/game/assets/img/frames/magic.png');
              game.load.image('frame/magic_with_cost', '/game/assets/img/frames/magic_with_cost.png');
              game.load.image('frame/elemental', '/game/assets/img/frames/element.png');
              game.load.image('frame/elemental_with_cost', '/game/assets/img/frames/elemental_with_cost.png');

              game.load.image('art/elemental', '/game/assets/img/art/elemental.png');
              game.load.image('art/fenrir', '/game/assets/img/art/fenrir.png');
              game.load.image('art/gridarvolr', '/game/assets/img/art/gridarvolr.png');
              game.load.image('art/hel', '/game/assets/img/art/hel.png');
              game.load.image('art/jarngreipr', '/game/assets/img/art/jarngreipr.png');
              game.load.image('art/jormungandr', '/game/assets/img/art/jormungandr.png');
              game.load.image('art/loki', '/game/assets/img/art/loki.png');
              game.load.image('art/magic', '/game/assets/img/art/magic.png');
              game.load.image('art/megingjord', '/game/assets/img/art/megingjord.png');
              game.load.image('art/mjolnir', '/game/assets/img/art/mjolnir.png');
              game.load.image('art/sexChange', '/game/assets/img/art/sex_change.png');
              game.load.image('art/thor', '/game/assets/img/art/thor.png');

              game.load.image('button', '/game/assets/img/button.png');
              game.load.image('newGameButton', '/game/assets/img/button-new-game.png');
              game.load.image('endTurnButton', '/game/assets/img/button-end-turn.png');
              game.load.image('background', '/game/assets/img/playing-field.png');
              game.load.image('thor-wins', '/game/assets/img/thor-wins-small.png');
              game.load.image('loki-wins', '/game/assets/img/win-screen-loki.png');
            }

            function createLog() {
              /*
              * newGameLog players, worldDeck, and buyDecks are the state they are at create of the game
              *
              *
              */
              var newGameLog = {};
              newGameLog.currentState = {};
              newGameLog.currentState.playersState = {};
              newGameLog.players = players;
              newGameLog.worldDeck = worldDeck;
              newGameLog.buyDecks = buyDecks;
              return newGameLog;
            }

            function create() {
              var background = game.add.tileSprite(0, 0, 1024, 768, 'background');
              background.visible = true;
              var menuBar = game.add.group();
              mainSpace = game.add.group();
              gameLog = createLog();
              endTurnButtonGroup = new Phaser.Group(game, menuBar);

              endTurnButton = endTurnButtonGroup.add(new Phaser.Button(game, 0, 0, 'endTurnButton', function() {
                endTurn();
              }));
              endTurnButton.width = 175;
              endTurnButton.height = 50;
              //endTurnButtonLabel = new Phaser.Text(game, 0, 0, 'End Turn');
              //var label = endTurnButtonGroup.add(endTurnButtonLabel);
              //label.x = endTurnButton.width / 2 - label.width / 2;
              //label.y = endTurnButton.height / 2 - label.height / 2;

              endTurnButtonGroup.x = game.width - endTurnButtonGroup.width;

              menuBar.x = 0;
              menuBar.y = 0;

              var readoutNames = ['magic', 'elemental', 'victoryPoints', 'turn', 'currentPlayer', 'worldsClaimed'];
              var readouts = menuBar.add(new Phaser.Group(game));
              var rightMost = 0;

              angular.forEach(readoutNames, function(name) {
                var readout = readouts.add(new Phaser.Group(game));
                var text = readout.add(new Phaser.Text(game, 0, 0, name+': ', {fill: 'white', font: '12px Arial'}));
                var display = '0';
                if(name === 'turn') {
                  display = '1';
                }
                var value = readout.add(new Phaser.Text(game, 0, 0, display, {fill: 'white', font: '12px Arial'}));
                counters[name] = value;
                value.x = text.x + text.width;
                readout.x = rightMost;
                rightMost = readout.x + readout.width + 25;
              });

              // ugggghhhhh
              mainSpace.maxX = game.width;
              mainSpace.maxY = menuBar.y = game.height - menuBar.height;
              cardHeight = mainSpace.maxY / 3.25;
              buyCardWidth = cardHeight * 0.67;
              cardWidth = cardHeight * 0.666;

              worldDeck = new Deck({type: 'world', topVisible: true});
              worldDeck.sprite = addCard('worldCardBack');
              worldDeck.sprite.x = game.width - cardWidth;
              worldDeck.counters = {};

              angular.forEach(['asgard', 'midgard', 'helWorld', 'jotunheim', 'niflheimr'], function(cardName) {
                worldDeck.addCard(new Card(cardTypes[cardName]));
              });

              worldCounter = mainSpace.add(new Phaser.Group(game));
              worldCounter.x = mainSpace.maxX - cardWidth * 2.5;

              var playerCounter = 0;
              angular.forEach(players, function(player) {
                worldDeck.counters[player.name] = {};
                var cardCounter = 0;
                player.createHook();
                angular.forEach(player.buyDecks, function(name) {
                  var offset = 0;
                  switch(cardCounter) {
                    case 0:
                      offset = -13;
                      break;
                    case 1:
                      offset = 0;
                      break;
                    case 2:
                      offset = 14;
                      break;
                    case 3:
                      offset = 28;
                      break;
                  }
                  var newBuyDeck = new Deck({fixedCardType: cardTypes[name], type: 'buy', x: (cardCounter * buyCardWidth) - offset, y: (playerCounter * cardHeight) + 23});
                  buyDecks.push(newBuyDeck);
                  cardCounter += 1;
                });

                var myWorldCounter = worldCounter.add(new Phaser.Group(game));
                myWorldCounter.x = cardWidth * 0.2;
                myWorldCounter.y = cardHeight * playerCounter + cardHeight * 0.1;
                angular.forEach(['magic', 'elemental'], function(type, index) {
                  var nameDisplay = myWorldCounter.add(new Phaser.Text(game, 0, 0, player.name, {fill: 'white'}));
                  var image = myWorldCounter.add(new Phaser.Image(game, cardWidth * 0.75, index * cardHeight * 0.375, type+'_icon'));
                  worldDeck.counters[player.name][type] = myWorldCounter.add(new Phaser.Text(game, image.x + image.width + 3, index * cardHeight * 0.375 + image.height / 2, '0', {fill: 'white', font: '16px Arial'}));
                });

                playerCounter += 1;
              });

              angular.forEach(buyDecks, function(deck) {
                deck.sprite = addCard('emptyDeck');
                deck.sprite.inputEnabled = true;
                deck.sprite.events.onInputUp.add(function(){ deck.purchase(); });
                deck.sprite.x = deck.x;
                deck.sprite.y = deck.y;
                for (var i=0; i < 10; i += 1) {
                  var buyCard = new Card(deck.fixedCardType);
                  //EnableDragAndDrop(buyCard.sprite);
                  deck.addCard(buyCard);
                }
                deck.sprite.destroy();
                deck.sprite = addCard(deck.fixedCardType.name);
                deck.sprite.inputEnabled = true;
                deck.sprite.events.onInputUp.add(function(){ deck.purchase(); });
                deck.sprite.x = deck.x;
                deck.sprite.y = deck.y;
              });
              worldsCount = worldDeck.cards.length;
              worldDeck.flipTop();

              setPlayer(players.Thor);
              gameLog.players = players;
              gameLog.worldDeck = worldDeck;
              gameLog.buyDecks = buyDecks;
              // TODO: leave this here, long term this needs to be done but but it's a deep rabbit hole.
              //localStorage.setObject("game1", gameLog);
              //console.log(localStorage.getObject("game1"));
            }

            function update() {}

            // ha, now game is used; jerk
            var game;
            game = new Phaser.Game(1024, 768, Phaser.AUTO, 'game', {preload: preload, create: create, update: update});
          }
      };
  });
