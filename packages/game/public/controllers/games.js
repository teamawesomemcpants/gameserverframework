'use strict';

angular.module('mean.game').controller('GamesController', ['$scope', '$stateParams', '$location', 'Global', 'Games',
  function($scope, $stateParams, $location, Global, Games) {
    $scope.global = Global;

    $scope.hasAuthorization = function(game) {
      if (!game || game.owner !== $scope.global.user._id || $scope.global.user.isAdmin === true) return false;
      return $scope.global.isAdmin || game.owner._id === $scope.global.user._id;
    };

    $scope.create = function(isValid) {
      if (isValid) {
        var game = new Games({
          title: this.title,
          description: this.description,
          player_limit: this.player_limit,
          is_private: this.is_private,
          password: this.password,
          ruleset: this.ruleset._id,
          owner: $scope.global.user._id
        });
        game.$save(function(response) {
          $location.path('game/' + response._id);
        });

        this.title = '';
        this.description = '';
        this.player_limit = '';
        this.is_private = '';
        this.password = '';
        this.confirmPassword = '';
        this.ruleset = null;
        this.owner = $scope.global.user;
      } else {
        $scope.submitted = true;
      }
    };

    $scope.remove = function(game) {
      if (game) {
        game.$remove();

        for (var i in $scope.games) {
          if ($scope.games[i] === game) {
            $scope.games.splice(i, 1);
          }
        }
      } else {
        $scope.game.$remove(function(response) {
          $location.path('game');
        });
      }
    };

    $scope.update = function(isValid) {
      if (isValid) {
        var game = $scope.game;
        if (!game.updated) {
          game.updated = [];
        }
        game.updated.push(new Date().getTime());

        game.$update(function() {
          $location.path('game/' + game._id);
        });
      } else {
        $scope.submitted = true;
      }
    };

    $scope.find = function() {
      Games.query(function(games) {
        $scope.games = games;
      });
    };

    $scope.findOne = function() {
      Games.get({
        gameId: $stateParams.gameId
      }, function(game) {
        $scope.game = game;
      });
    };
  }
]);
