'use strict';

//Games service used for games REST endpoint
angular.module('mean.game').factory('Games', ['$resource',
  function($resource) {
    return $resource('game/:gameId', {
      gameId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
