'use strict';

//Setting up route
angular.module('mean.game').config(['$stateProvider',
  function($stateProvider) {
    // Check if the user is connected
    var checkLoggedin = function($q, $timeout, $http, $location) {
      // Initialize a new promise
      var deferred = $q.defer();

      // Make an AJAX call to check if the user is logged in
      $http.get('/loggedin').success(function(user) {
        // Authenticated
        if (user !== '0') $timeout(deferred.resolve);

        // Not Authenticated
        else {
          //$timeout(deferred.reject);
          //$location.url('/login');
          console.log('not logged in');
        }
      });

      return deferred.promise;
    };

    // states for my app
    $stateProvider
      .state('all Games', {
        url: '/game',
        templateUrl: 'game/views/list.html',
        resolve: {
          loggedin: checkLoggedin
        }
      })
      .state('create game', {
        url: '/game/create',
        templateUrl: 'game/views/create.html',
        resolve: {
          loggedin: checkLoggedin
        }
      })
      .state('edit game', {
        url: '/game/:gameId/edit',
        templateUrl: 'game/views/edit.html',
        resolve: {
          loggedin: checkLoggedin
        }
      })
      .state('game by id', {
        url: '/game/:gameId',
        templateUrl: 'game/views/view.html',
        resolve: {
          loggedin: checkLoggedin
        }
      })
      .state('play', {
        url: '/game/:gameId/play',
        templateUrl: 'game/views/play.html'
      });
  }
]);
