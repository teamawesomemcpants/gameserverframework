'use strict';

(function() {
  // RuleSets Controller Spec
  describe('MEAN controllers', function() {
    describe('RuleSetsController', function() {
      // The $resource service augments the response object with methods for updating and deleting the resource.
      // If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
      // the responses exactly. To solve the problem, we use a newly-defined toEqualData Jasmine matcher.
      // When the toEqualData matcher compares two objects, it takes only object properties into
      // account and ignores methods.
      beforeEach(function() {
        this.addMatchers({
          toEqualData: function(expected) {
            return angular.equals(this.actual, expected);
          }
        });
      });

      beforeEach(function() {
        module('mean');
        module('mean.system');
        module('mean.ruleset');
      });

      // Initialize the controller and a mock scope
      var RuleSetsController,
        scope,
        $httpBackend,
        $stateParams,
        $location;

      // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
      // This allows us to inject a service but then attach it to a variable
      // with the same name as the service.
      beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {

        scope = $rootScope.$new();

        RuleSetsController = $controller('RuleSetsController', {
          $scope: scope
        });

        $stateParams = _$stateParams_;

        $httpBackend = _$httpBackend_;

        $location = _$location_;

      }));

      it('$scope.find() should create an array with at least one ruleset object ' +
        'fetched from XHR', function() {

          // test expected GET request
          $httpBackend.expectGET('rulesets').respond([{
            title: 'An RuleSet',
            custom_state:{ test: 'MEAN rocks!'}
          }]);

          // run controller
          scope.find();
          $httpBackend.flush();

          // test scope value
          expect(scope.rulesets).toEqualData([{
            title: 'An RuleSet',
            custom_state:{ test: 'MEAN rocks!'}
          }]);

        });

      it('$scope.findOne() should create an array with one ruleset object fetched ' +
        'from XHR using a rulesetId URL parameter', function() {
          // fixture URL parament
          $stateParams.rulesetId = '525a8422f6d0f87f0e407a33';

          // fixture response object
          var testRuleSetData = function() {
            return {
              title: 'An RuleSet',
              custom_state:{ test: 'MEAN rocks!'}
            };
          };

          // test expected GET request with response object
          $httpBackend.expectGET(/ruleset\/([0-9a-fA-F]{24})$/).respond(testRuleSetData());

          // run controller
          scope.findOne();
          $httpBackend.flush();

          // test scope value
          expect(scope.ruleset).toEqualData(testRuleSetData());

        });

      it('$scope.create() with valid form data should send a POST request ' +
        'with the form input values and then ' +
        'locate to new object URL', function() {

          // fixture expected POST data
          var postRuleSetData = function() {
            return {
              title: 'An RuleSet',
              custom_state:{ test: 'MEAN rocks!'}
            };
          };

          // fixture expected response data
          var responseRuleSetData = function() {
            return {
              _id: '525cf20451979dea2c000001',
              title: 'An RuleSet',
              custom_state:{ test: 'MEAN rocks!'}
            };
          };

          // fixture mock form input values
          scope.title = 'An RuleSet';
          scope.custom_state = { test: 'MEAN rocks!'};

          // test post request is sent
          $httpBackend.expectPOST('rulesets', postRuleSetData()).respond(responseRuleSetData());

          // Run controller
          scope.create(true);
          $httpBackend.flush();

          // test form input(s) are reset
          expect(scope.title).toEqual('');
          expect(scope.custom_state).toEqual('');

          // test URL location to new object
          expect($location.path()).toBe('/ruleset/' + responseRuleSetData()._id);
        });

      it('$scope.update(true) should update a valid ruleset', inject(function(Rulesets) {

        // fixture rideshare
        var putRuleSetData = function() {
          return {
            _id: '525a8422f6d0f87f0e407a33',
            title: 'An RuleSet',
            custom_state:{ test: 'MEAN rocks!'}
          };
        };

        // mock ruleset object from form
        var ruleset = new Rulesets(putRuleSetData());

        // mock ruleset in scope
        scope.ruleset = ruleset;

        // test PUT happens correctly
        $httpBackend.expectPUT(/ruleset\/([0-9a-fA-F]{24})$/).respond();

        // testing the body data is out for now until an idea for testing the dynamic updated array value is figured out
        //$httpBackend.expectPUT(/rulesets\/([0-9a-fA-F]{24})$/, putRuleSetData()).respond();
        /*
                Error: Expected PUT /rulesets\/([0-9a-fA-F]{24})$/ with different data
                EXPECTED: {"_id":"525a8422f6d0f87f0e407a33","title":"An RuleSet about MEAN","to":"MEAN is great!"}
                GOT:      {"_id":"525a8422f6d0f87f0e407a33","title":"An RuleSet about MEAN","to":"MEAN is great!","updated":[1383534772975]}
                */

        // run controller
        scope.update(true);
        $httpBackend.flush();

        // test URL location to new object
        expect($location.path()).toBe('/ruleset/' + putRuleSetData()._id);

      }));

      it('$scope.remove() should send a DELETE request with a valid rulesetId ' +
        'and remove the ruleset from the scope', inject(function(Rulesets) {

          // fixture rideshare
          var RuleSet = new Rulesets({
            _id: '525a8422f6d0f87f0e407a33'
          });

          // mock rideshares in scope
          scope.rulesets = [];
          scope.rulesets.push(RuleSet);

          // test expected rideshare DELETE request
          $httpBackend.expectDELETE(/ruleset\/([0-9a-fA-F]{24})$/).respond(204);

          // run controller
          scope.remove(RuleSet);
          $httpBackend.flush();

          // test after successful delete URL location rulesets list
          //expect($location.path()).toBe('/rulesets');
          expect(scope.rulesets.length).toBe(0);

        }));
    });
  });
}());
