'use strict';

//RuleSets service used for rulesets REST endpoint
angular.module('mean.ruleset').factory('Rulesets', ['$resource',
  function($resource) {
    return $resource('ruleset/:rulesetId', {
      rulesetId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
