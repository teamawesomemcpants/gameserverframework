'use strict';

angular.module('mean.ruleset').controller('RuleSetsController', ['$scope', '$stateParams', '$location', 'Global', 'Rulesets',
  function($scope, $stateParams, $location, Global, RuleSets) {
    $scope.global = Global;

    $scope.hasAuthorization = function(ruleset) {
      if (!ruleset) return false;
      return $scope.global.isAdmin;
    };

    $scope.create = function(isValid) {
      if (isValid) {
        var ruleset = new RuleSets({
          title: this.title,
          custom_state: this.content
        });
        ruleset.$save(function(response) {
          $location.path('ruleset/' + response._id);
        });

        this.title = '';
        this.custom_state = '';
      } else {
        $scope.submitted = true;
      }
    };

    $scope.remove = function(ruleset) {
      if (ruleset) {
        ruleset.$remove();

        for (var i in $scope.rulesets) {
          if ($scope.rulesets[i] === ruleset) {
            $scope.rulesets.splice(i, 1);
          }
        }
      } else {
        $scope.ruleset.$remove(function(response) {
          $location.path('ruleset');
        });
      }
    };

    $scope.update = function(isValid) {
      if (isValid) {
        var ruleset = $scope.ruleset;
        if (!ruleset.updated) {
          ruleset.updated = [];
        }
        ruleset.updated.push(new Date().getTime());

        ruleset.$update(function() {
          $location.path('ruleset/' + ruleset._id);
        });
      } else {
        $scope.submitted = true;
      }
    };

    $scope.find = function() {
      RuleSets.query(function(rulesets) {
        $scope.rulesets = rulesets;
      });
    };

    $scope.findOne = function() {
      RuleSets.get({
        rulesetId: $stateParams.rulesetId
      }, function(ruleset) {
        $scope.ruleset = ruleset;
      });
    };
  }
]);
