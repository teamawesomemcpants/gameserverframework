'use strict';

var rulesets = require('../controllers/rulesets');

// RuleSet authorization helpers
var hasAuthorization = function(req, res, next) {
  if (!req.user.isAdmin) {
    return res.send(401, 'User is not authorized');
  }
  next();
};

module.exports = function(RuleSets, app, auth) {

  app.route('/ruleset')
    .get(rulesets.all)
    .post(auth.requiresLogin, rulesets.create);
  app.route('/ruleset/:rulesetId')
    .get(rulesets.show)
    .put(auth.requiresLogin, hasAuthorization, rulesets.update)
    .delete(auth.requiresLogin, hasAuthorization, rulesets.destroy);

  // Finish with setting up the rulesetId param
  app.param('rulesetId', rulesets.ruleset);
};
