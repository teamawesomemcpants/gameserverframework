'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  RuleSet = mongoose.model('RuleSet'),
  _ = require('lodash');


/**
 * Find ruleset by id
 */
exports.ruleset = function(req, res, next, id) {
  RuleSet.load(id, function(err, ruleset) {
    if (err) return next(err);
    if (!ruleset) return next(new Error('Failed to load ruleset ' + id));
    req.ruleset = ruleset;
    next();
  });
};

/**
 * Create an ruleset
 */
exports.create = function(req, res) {
  var ruleset = new RuleSet(req.body);
  ruleset.user = req.user;

  ruleset.save(function(err) {
    if (err) {
      return res.json(500, {
        error: 'Cannot save the ruleset'
      });
    }
    res.json(ruleset);

  });
};

/**
 * Update an ruleset
 */
exports.update = function(req, res) {
  var ruleset = req.ruleset;

  ruleset = _.extend(ruleset, req.body);

  ruleset.save(function(err) {
    if (err) {
      return res.json(500, {
        error: 'Cannot update the ruleset'
      });
    }
    res.json(ruleset);

  });
};

/**
 * Delete an ruleset
 */
exports.destroy = function(req, res) {
  var ruleset = req.ruleset;

  ruleset.remove(function(err) {
    if (err) {
      return res.json(500, {
        error: 'Cannot delete the ruleset'
      });
    }
    res.json(ruleset);

  });
};

/**
 * Show an ruleset
 */
exports.show = function(req, res) {
  res.json(req.ruleset);
};

/**
 * List of RuleSets
 */
exports.all = function(req, res) {
  RuleSet.find().sort('-created').populate('user', 'name username').exec(function(err, rulesets) {
    if (err) {
      return res.json(500, {
        error: 'Cannot list the rulesets'
      });
    }
    res.json(rulesets);

  });
};
