'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
  mongoose = require('mongoose'),
  Ruleset = mongoose.model('RuleSet');

/**
 * Globals
 */
var ruleset;

/**
 * Test Suites
 */
describe('<Unit Test>', function() {
  describe('Model Ruleset:', function() {
    beforeEach(function(done) {
        ruleset = new Ruleset({
          title: 'Ruleset Title',
          custom_state: {
            hand_size: 5,
            discard_rest_of_hand: true,
            shuffle_required: false,
            max_deck_size: 0, //0 means unlimited
            resources_to_spend: {
              coins: true,
              hearts: true,
            },
            resource_limits: {
              unlimited: true
            }
          },
          async: false,
          player_interaction: false,
          valid_timelimits: [0.5, 1, 5, 10, 30, 60, 1440, 10080, 30240],
          limit_turn_actions: false
        });
        done();
      });

    describe('Method Save', function() {
      it('should be able to save without problems', function(done) {
        ruleset.markModified('custom_state');
        return ruleset.save(function(err) {
          should.not.exist(err);
          ruleset.title.should.equal('Ruleset Title');
          ruleset.async.should.equal(false);
          ruleset.player_interaction.should.equal(false);
          ruleset.valid_timelimits.should.have.length(9);
          ruleset.limit_turn_actions.should.equal(false);
          ruleset.custom_state.should.not.have.length(0);
          ruleset.created.should.not.have.length(0);
          done();
        });
      });

      it('should be able to show an error when try to save without title', function(done) {
        ruleset.title = '';

        return ruleset.save(function(err) {
          should.exist(err);
          done();
        });
      });

      it('should be able to show an error when try to save without custom_state', function(done) {
        ruleset.custom_state = '';
        ruleset.markModified('custom_state'); //Free form fields must be marked as modified for mongoose to save them
        return ruleset.save(function(err) {
          should.exist(err);
          done();
        });
      });

    });

    afterEach(function(done) {
      ruleset.remove();
      done();
    });
  });
});
