'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;


/**
 * RuleSet Schema
 */
var RuleSetSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  title: {
    type: String,
    required: true,
    trim: true
  },
  custom_state: {
  },
  async: {
    type: Boolean,
    default: false
  },
  valid_timelimits: [
    {
      type: Number
    }
  ],
  limit_turn_actions: {
    type: Boolean,
    default: false
  },
  player_interaction: {
    type: Boolean,
    default: false
  }
});

/**
 * Validations
 */
RuleSetSchema.path('title').validate(function(title) {
  return !!title;
}, 'Title cannot be blank');

RuleSetSchema.path('custom_state').validate(function(custom_state) {
  return !!custom_state;
}, 'Custom State cannot be blank');

/**
 * Statics
 */
RuleSetSchema.statics.load = function(id, cb) {
  this.findOne({
    _id: id
  }).exec(cb);
};

mongoose.model('RuleSet', RuleSetSchema);
